// The MIT License (MIT)
// 
// Copyright (c) 2016, Gergely Nagy
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include <util/hash.h>
#include <util/tree.h>
#include <util/stack.h>
#include <util/logging.h>
#include <util/testing.h>


uint32_t hash_function(int k) {return k % 5;}
bool is_equal(int k1, int k2) {
	log_5("Comparing %d and %d", k1, k2);
	return k1 == k2;
}
void print_all(int key, int value) {printf("Key: %d\tValue: %d\n", key, value);}

bool test_0(void) {
	struct util_hash_int_int h = util_hash_alloc_def(int, int);

	util_hash_int_int_foreach(&h, print_all);

	util_hash_int_int_add(&h, 4, 0);
	util_hash_int_int_add(&h, 9, 1);
	util_hash_int_int_add(&h, 14, 2);
	util_hash_int_int_add(&h, 19, 3);
	util_hash_int_int_add(&h, 24, 4);
	util_hash_int_int_add(&h, 29, 5);

	util_hash_int_int_foreach(&h, print_all);
	printf("\n");

	return true;
}

bool test_1(void) {
	struct util_hash_int_int h = util_hash_alloc(int, int, 5, hash_function, is_equal);

	util_hash_int_int_foreach(&h, print_all);

	util_hash_int_int_add(&h, 4, 0);
	util_hash_int_int_add(&h, 9, 1);
	util_hash_int_int_add(&h, 14, 2);
	util_hash_int_int_add(&h, 19, 3);
	util_hash_int_int_add(&h, 24, 4);
	util_hash_int_int_add(&h, 29, 5);

	util_hash_int_int_foreach(&h, print_all);
	printf("\n");

	return true;
}

bool test_2(void) {
	struct util_hash_int_int h = util_hash_alloc(int, int, 5, hash_function);

	util_hash_int_int_foreach(&h, print_all);

	util_hash_int_int_add(&h, 4, 0);
	util_hash_int_int_add(&h, 9, 1);
	util_hash_int_int_add(&h, 14, 2);
	util_hash_int_int_add(&h, 19, 3);
	util_hash_int_int_add(&h, 24, 4);
	util_hash_int_int_add(&h, 29, 5);

	util_hash_int_int_foreach(&h, print_all);
	printf("\n");

	return true;
}
bool test_3(void) {
	struct util_hash_int_int h = util_hash_alloc(int, int, 5);

	util_hash_int_int_foreach(&h, print_all);

	util_hash_int_int_add(&h, 4, 0);
	util_hash_int_int_add(&h, 9, 1);
	util_hash_int_int_add(&h, 14, 2);
	util_hash_int_int_add(&h, 19, 3);
	util_hash_int_int_add(&h, 24, 4);
	util_hash_int_int_add(&h, 29, 5);

	util_hash_int_int_foreach(&h, print_all);
	printf("\n");

	return true;
}

void print_node(float *value, int level) {
	for (int i = 0; i < level; ++i) printf("\t");
	printf("%lf\n", *value);
}

bool test_4(void) {
	struct util_tree_float *t = util_tree_float_alloc(0.0f);
	struct util_tree_float *new_element;

	new_element = util_tree_float_add_existing_child(t, util_tree_float_alloc(1.0f));
	new_element = util_tree_float_add_existing_child(new_element, util_tree_float_alloc(2.0f));
	new_element = util_tree_float_add_existing_child(new_element, util_tree_float_alloc(3.0f));

	new_element = util_tree_float_add_existing_sibling(new_element, util_tree_float_alloc(4.0f));
	new_element = util_tree_float_add_existing_sibling(new_element, util_tree_float_alloc(5.0f));
	new_element = util_tree_float_add_existing_sibling(new_element, util_tree_float_alloc(6.0f));

	util_tree_float_add_existing_sibling(t, util_tree_float_alloc(7.0f));
	util_tree_float_add_existing_sibling(t, util_tree_float_alloc(8.0f));
	util_tree_float_add_existing_sibling(t, util_tree_float_alloc(9.0f));

	util_tree_float_foreach(t, print_node);

	util_tree_float_free(t);

	return true;
}

void print_string_node(char **value, int level) {
	for (int i = 0; i < level; ++i) printf("\t");
	printf("%s\n", *value);
}

bool test_5(void) {
	struct util_tree_charptr *t = util_tree_charptr_alloc("apple");
	struct util_tree_charptr *new_element;

	new_element = util_tree_charptr_add_existing_child(t, util_tree_charptr_alloc("banana"));
	new_element = util_tree_charptr_add_existing_child(new_element, util_tree_charptr_alloc("cherry"));
	new_element = util_tree_charptr_add_existing_child(new_element, util_tree_charptr_alloc("date"));

	new_element = util_tree_charptr_add_existing_sibling(new_element, util_tree_charptr_alloc("elderberry"));
	new_element = util_tree_charptr_add_existing_sibling(new_element, util_tree_charptr_alloc("fig"));
	new_element = util_tree_charptr_add_existing_sibling(new_element, util_tree_charptr_alloc("grape"));

	util_tree_charptr_add_existing_sibling(t, util_tree_charptr_alloc("huckleberry"));
	util_tree_charptr_add_existing_sibling(t, util_tree_charptr_alloc("iceberg lettuce"));
	util_tree_charptr_add_existing_sibling(t, util_tree_charptr_alloc("jalapeno"));

	util_tree_charptr_foreach(t, print_string_node);

	util_tree_charptr_free(t);

	return true;
}

bool test_6(void) {
	struct util_stack_int s = util_stack_int_create();

	for (int i = 0; i < 10; ++i)
		util_stack_int_push(&s, i);

	int *value;
	while ((value = util_stack_int_top(&s)) != NULL) {
		printf("%d\n", *value);
		util_stack_int_pop(&s);
	}

	return true;
}

int main(void) {
	bool (*tests[])(void) = {
		test_0, test_1, test_2, test_3, test_4, test_5, test_6,
		NULL
	};

	util_testing_run(tests);

	return 0;
}

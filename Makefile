@phony: all all5 run clean

all:
	gcc -std=c99 -Iifs -Idefs -o test test.c `find src -name "*.c"`

all5:
	gcc -std=c99 -Iifs -Idefs -D LOG=5 -o test test.c  `find src -name "*.c"`

run:
	./test

clean:
	rm -f test

// The MIT License (MIT)
// 
// Copyright (c) 2016, Gergely Nagy
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
#ifndef UTIL_DEFAULT_ARGS
#define UTIL_DEFAULT_ARGS

#define get_function_2(_1, _2, name, ...) name
#define get_function_3(_1, _2, _3, name, ...) name
#define get_function_4(_1, _2, _3, _4, name, ...) name
#define get_function_5(_1, _2, _3, _4, _5, name, ...) name
#define get_function_6(_1, _2, _3, _4, _5, _6, name, ...) name
#define get_function_7(_1, _2, _3, _4, _5, _6, _7, name, ...) name
#define get_function_8(_1, _2, _3, _4, _5, _6, _7, _8, name, ...) name
#define get_function_9(_1, _2, _3, _4, _5, _6, _7, _8, _9, name, ...) name
#define get_function_10(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, name, ...) name

#endif

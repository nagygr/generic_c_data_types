// The MIT License (MIT)
// 
// Copyright (c) 2016, Gergely Nagy
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
#ifndef UTIL_LOGGING
#define UTIL_LOGGING

#include <stdio.h>
#include <stdbool.h>

/*
 * Compile with -D LOG=<log level> to enable logging
 * Level 1 -- E: error
 * Level 2 -- W: warning
 * Level 3 -- I: information (preferably restricted to state changes)
 * Level 4 -- D: debug (details of the operation)
 * Level 5 -- V: verbose (fine details of the operation)
 */

#define log_1(...) 
#define log_2(...) 
#define log_3(...) 
#define log_4(...) 
#define log_5(...) 

#if LOG >= 1
#	undef log_1
#	define log_1(...) do {fprintf(stderr, "[%s %s] [E] ",  __DATE__, __TIME__); fprintf(stderr, __VA_ARGS__); fprintf(stderr, " (%s:%d (%s))\n", __FILE__, __LINE__, __FUNCTION__);} while (false)
#endif

#if LOG >= 2
#	undef log_2
#	define log_2(...) do {fprintf(stderr, "[%s %s] [W] ",  __DATE__, __TIME__); fprintf(stderr, __VA_ARGS__); fprintf(stderr, " (%s:%d (%s))\n", __FILE__, __LINE__, __FUNCTION__);} while (false)
#endif

#if LOG >= 3
#	undef log_3
#	define log_3(...) do {fprintf(stderr, "[%s %s] [I] ",  __DATE__, __TIME__); fprintf(stderr, __VA_ARGS__); fprintf(stderr, " (%s:%d (%s))\n", __FILE__, __LINE__, __FUNCTION__);} while (false)
#endif


#if LOG >= 4
#	undef log_4
#	define log_4(...) do {fprintf(stderr, "[%s %s] [D] ",  __DATE__, __TIME__); fprintf(stderr, __VA_ARGS__); fprintf(stderr, " (%s:%d (%s))\n", __FILE__, __LINE__, __FUNCTION__);} while (false)
#endif


#if LOG >= 5
#	undef log_5
#	define log_5(...) do {fprintf(stderr, "[%s %s] [V] ",  __DATE__, __TIME__); fprintf(stderr, __VA_ARGS__); fprintf(stderr, " (%s:%d (%s))\n", __FILE__, __LINE__, __FUNCTION__);} while (false)
#endif

#endif

// The MIT License (MIT)
// 
// Copyright (c) 2016, Gergely Nagy
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
#ifndef UTIL_TREE
#define UTIL_TREE

#define create_tree(type) \
struct util_tree_##type { \
	type value; \
	struct util_tree_##type *sibling; \
	struct util_tree_##type *child; \
}; \
 \
struct util_tree_##type *util_tree_##type##_alloc_empty(void); \
struct util_tree_##type *util_tree_##type##_alloc(type value); \
 \
struct util_tree_##type *util_tree_##type##_add_existing_child(struct util_tree_##type *root, struct util_tree_##type *child); \
struct util_tree_##type *util_tree_##type##_add_existing_sibling(struct util_tree_##type *root, struct util_tree_##type *sibling); \
 \
void util_tree_##type##_free(struct util_tree_##type *root); \
 \
void util_tree_##type##_foreach(struct util_tree_##type *root, void (*operation)(type *, int)); \
void util_tree_##type##_foreach_helper(struct util_tree_##type *root, void (*operation)(type *, int), int level); \

#include <util/tree.def>

#undef create_tree

#endif

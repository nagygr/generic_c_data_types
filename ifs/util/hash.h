// The MIT License (MIT)
// 
// Copyright (c) 2016, Gergely Nagy
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
#ifndef UTIL_HASH
#define UTIL_HASH

#include <stdint.h>
#include <stdbool.h>

#include <util/default_args.h>

#define create_hash_map(key, value) \
struct util_hash_element_##key##_##value; \
 \
struct util_hash_##key##_##value { \
	struct util_hash_element_##key##_##value *array; \
	uint32_t capacity; \
 \
	uint32_t (*hash_function)(key); \
	bool (*is_equal)(key, key); \
}; \
 \
struct util_hash_##key##_##value util_hash_##key##_##value##_alloc_default(void); \
struct util_hash_##key##_##value util_hash_##key##_##value##_alloc_initial(uint32_t initial_capacity); \
struct util_hash_##key##_##value util_hash_##key##_##value##_alloc_hash(uint32_t initial_capacity, uint32_t (*hash_function)(key)); \
struct util_hash_##key##_##value util_hash_##key##_##value##_alloc_hash_equal(uint32_t initial_capacity, uint32_t (*hash_function)(key), bool (*is_equal)(key, key)); \
bool util_hash_##key##_##value##_add(struct util_hash_##key##_##value *self, key the_key, value the_value); \
value *util_hash_##key##_##value##_get(struct util_hash_##key##_##value *self, key the_key); \
void util_hash_##key##_##value##_foreach(struct util_hash_##key##_##value *self, void (*operation)(key, value)); \
void util_hash_##key##_##value##_free(struct util_hash_##key##_##value *self); \
 
#include <util/hash.def> 
 
#undef create_hash_map

#define util_hash_alloc(key, value, ...) \
	get_function_3(__VA_ARGS__, util_hash_##key##_##value##_alloc_hash_equal, util_hash_##key##_##value##_alloc_hash, util_hash_##key##_##value##_alloc_initial)(__VA_ARGS__)

#define util_hash_alloc_def(key, value) \
	util_hash_##key##_##value##_alloc_default()
#endif

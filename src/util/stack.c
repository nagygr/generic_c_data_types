// The MIT License (MIT)
// 
// Copyright (c) 2016, Gergely Nagy
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
#include <stdlib.h>
#include <stddef.h>

#include <util/stack.h>

#define create_stack(type) \
struct util_stack_##type##_element { \
	type value; \
	struct util_stack_##type##_element *next; \
}; \
 \
struct util_stack_##type util_stack_##type##_create(void) { \
	return (struct util_stack_##type) { \
		.stack = NULL \
	}; \
} \
 \
bool util_stack_##type##_push(struct util_stack_##type *stack, type value) { \
	struct util_stack_##type##_element *new_element = (struct util_stack_##type##_element *)malloc(sizeof(struct util_stack_##type##_element)); \
 \
	if (new_element != NULL) { \
		new_element->value = value; \
		new_element->next = stack->stack; \
		stack->stack = new_element; \
		return true; \
	} \
	else { \
		return false; \
	} \
} \
 \
type *util_stack_##type##_top(struct util_stack_##type *stack) { \
	if (stack->stack == NULL) return NULL; \
	return &stack->stack->value; \
} \
 \
bool util_stack_##type##_pop(struct util_stack_##type *stack) { \
	if (stack->stack == NULL) return false; \
 \
	struct util_stack_##type##_element *top = stack->stack; \
	struct util_stack_##type##_element *new_top = top->next; \
 \
	free(top); \
	stack->stack = new_top; \
	return true; \
}

#include <util/stack.def>

#undef create_stack


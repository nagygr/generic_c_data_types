// The MIT License (MIT)
// 
// Copyright (c) 2016, Gergely Nagy
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
#include <stddef.h>
#include <stdlib.h>

#include <util/tree.h>
#include <util/logging.h>

#define create_tree(type) \
struct util_tree_##type *util_tree_##type##_alloc_empty(void) { \
	log_5("Allocating empty tree node"); \
 \
	struct util_tree_##type *new_instance = (struct util_tree_##type *)malloc(sizeof(struct util_tree_##type)); \
 \
	if (new_instance != NULL) { \
		new_instance->sibling = new_instance->child = NULL; \
	} \
 \
 	log_5("New tree node is: %p", new_instance); \
	return new_instance; \
} \
 \
struct util_tree_##type *util_tree_##type##_alloc(type value) { \
	log_5("Allocating tree node"); \
 \
	struct util_tree_##type *new_instance = util_tree_##type##_alloc_empty(); \
 \
	if (new_instance != NULL) { \
		new_instance->value = value; \
	} \
 \
 	log_5("New tree node is: %p", new_instance); \
	return new_instance; \
} \
 \
struct util_tree_##type *util_tree_##type##_add_existing_child(struct util_tree_##type *root, struct util_tree_##type *child) { \
	log_5("Adding new child (%p) to node (%p)", child, root); \
 \
	if (root != NULL) { \
		if (root->child == NULL) { \
			root->child = child; \
		} \
		else { \
			struct util_tree_##type *node; \
			for (node = root->child; node->sibling != NULL; node = node->sibling); \
			node->sibling = child; \
		} \
	} \
 \
	return child; \
} \
 \
struct util_tree_##type *util_tree_##type##_add_existing_sibling(struct util_tree_##type *root, struct util_tree_##type *sibling) { \
	log_5("Adding new sibling (%p) to node (%p)", sibling, root); \
 \
	if (root != NULL && sibling != NULL) { \
		if (root->sibling == NULL) { \
			root->sibling = sibling; \
		} \
		else { \
			struct util_tree_##type *node; \
			for (node = root->sibling; node->sibling != NULL; node = node->sibling); \
			node->sibling = sibling; \
		} \
	} \
 \
	return sibling; \
} \
 \
void util_tree_##type##_free(struct util_tree_##type *root) { \
	log_5("Freeing node (%p)", root); \
 \
	if (root != NULL) { \
		util_tree_##type##_free(root->child); \
		util_tree_##type##_free(root->sibling); \
		free(root); \
	} \
 \
	log_5("Node at %p freed", root); \
} \
 \
void util_tree_##type##_foreach(struct util_tree_##type *root, void (*operation)(type *, int)) { \
	util_tree_##type##_foreach_helper(root, operation, 0); \
} \
 \
void util_tree_##type##_foreach_helper(struct util_tree_##type *root, void (*operation)(type *, int), int level) { \
	if (root != NULL) { \
		log_5("Tree foreach at level %d on node %p", level, root); \
		operation(&(root->value), level); \
 \
		util_tree_##type##_foreach_helper(root->child, operation, level + 1); \
		util_tree_##type##_foreach_helper(root->sibling, operation, level); \
	} \
} \

#include <util/tree.def>

#undef create_tree

// The MIT License (MIT)
// 
// Copyright (c) 2016, Gergely Nagy
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
#include <stddef.h>
#include <stdlib.h>
#include <inttypes.h>

#include <util/logging.h>
#include <util/hash.h>

static uint32_t initial_size = 10;

#define create_hash_map(key, value) \
struct util_hash_element_##key##_##value { \
	key the_key; \
	value the_value; \
	bool occupied; \
}; \
 \
static bool util_hash_##key##_##value##_default_is_equal(key lhs, key rhs) { \
	return lhs == rhs; \
} \
 \
static uint32_t util_hash_##key##_##value##_default_hash(key the_key) { \
	void const *ptr = &the_key; \
	return *(uint32_t *)ptr; \
} \
 \
static struct util_hash_##key##_##value util_hash_##key##_##value##_create_empty(void) { \
	return (struct util_hash_##key##_##value){ \
		.array = NULL, \
		.capacity = 0, \
		.hash_function = util_hash_##key##_##value##_default_hash, \
		.is_equal = util_hash_##key##_##value##_default_is_equal \
	}; \
} \
 \
static bool util_hash_##key##_##value##_realloc(struct util_hash_##key##_##value *self, uint32_t new_capacity); \
 \
struct util_hash_##key##_##value util_hash_##key##_##value##_alloc_default(void) { \
	return util_hash_##key##_##value##_alloc_initial(initial_size); \
} \
 \
struct util_hash_##key##_##value util_hash_##key##_##value##_alloc_initial(uint32_t initial_capacity) { \
	struct util_hash_##key##_##value hash_map = util_hash_##key##_##value##_create_empty(); \
 \
	/* calloc is needed to initialize .occupied to 0 (== false) */ \
	hash_map.array = (struct util_hash_element_##key##_##value *)(calloc(initial_capacity, sizeof(struct util_hash_element_##key##_##value))); \
 \
	if (hash_map.array != NULL) { \
		hash_map.capacity = initial_capacity; \
	} \
	else { \
		log_1("Couldn't allocate memory of size (%u * %" PRIuPTR " = %" PRIuPTR " bytes) for a hash map.", initial_capacity, sizeof(struct util_hash_element_##key##_##value), initial_capacity * sizeof(struct util_hash_element_##key##_##value)); \
	} \
 \
	return hash_map; \
} \
struct util_hash_##key##_##value util_hash_##key##_##value##_alloc_hash(uint32_t initial_capacity, uint32_t (*hash_function)(key)) { \
	struct util_hash_##key##_##value hash_map = util_hash_##key##_##value##_alloc_initial(initial_capacity); \
 \
	if (hash_function != NULL) hash_map.hash_function = hash_function; \
 \
	return hash_map; \
} \
 \
struct util_hash_##key##_##value util_hash_##key##_##value##_alloc_hash_equal(uint32_t initial_capacity, uint32_t (*hash_function)(key), bool (*is_equal)(key, key)) { \
	struct util_hash_##key##_##value hash_map = util_hash_##key##_##value##_alloc_initial(initial_capacity); \
 \
	if (hash_function != NULL) hash_map.hash_function = hash_function; \
	if (is_equal      != NULL) hash_map.is_equal      = is_equal; \
 \
	return hash_map; \
} \
 \
bool util_hash_##key##_##value##_add(struct util_hash_##key##_##value *self, key the_key, value the_value) { \
	if (self->array == NULL || self->capacity == 0) { \
		log_1("Hash map's array is a NULL pointer or capacity is zero"); \
		return false; \
	} \
 \
	uint32_t hash = self->hash_function(the_key); \
	uint32_t initial_index = hash % self->capacity; \
	uint32_t index = initial_index; \
 \
 	log_5("Trying to add new element to index: %d", index); \
 \
	if (self->array[initial_index].occupied) { \
		log_5("Key collision detected"); \
 \
		bool has_key = self->is_equal(self->array[initial_index].the_key, the_key); \
 \
		if (!has_key) \
			for (index = (index + 1) % self->capacity; index != initial_index && self->array[index].occupied == true && !(has_key = self->is_equal(self->array[index].the_key, the_key)); index = (index + 1) % self->capacity) { \
				log_5("Current element at %d is %s", index, self->array[index].occupied ? "occupied" : "vacant"); \
			} \
 \
		if (has_key) { \
			log_5("An element with the given key is already in the map, overwriting value at index: %d", index); \
			self->array[index].the_value = the_value; \
			return true; \
		} \
		else if (index == initial_index) { \
			if (!util_hash_##key##_##value##_realloc(self, self->capacity * 2)) { \
				log_1("The hash map is full and couldn't enlarge it");  \
				return false; \
			} \
			else { \
				log_5("Adding collided element after reallocation"); \
				return util_hash_##key##_##value##_add(self, the_key, the_value); \
			}  \
		} \
	} \
 \
	log_5("Adding new element to the hash map at index: %d", index); \
	struct util_hash_element_##key##_##value *element = &self->array[index]; \
	element->the_key = the_key; \
	element->the_value = the_value; \
	element->occupied = true; \
 \
	return true; \
} \
 \
value *util_hash_##key##_##value##_get(struct util_hash_##key##_##value *self, key the_key) { \
	uint32_t initial_index = self->hash_function(the_key) % self->capacity; \
	uint32_t index = initial_index; \
 \
	if (!self->is_equal(self->array[index].the_key, the_key)) { \
		for (++index; index != initial_index && !self->is_equal(self->array[index].the_key, the_key); index = (index + 1) % self->capacity); \
 \
		if (index == initial_index) { \
			log_5("The element was not found in the hash_map"); \
			return NULL; \
		} \
	} \
 \
	return &(self->array[index].the_value); \
} \
 \
void util_hash_##key##_##value##_foreach(struct util_hash_##key##_##value *self, void (*operation)(key, value)) { \
	for (int i = 0; i < self->capacity; ++i) { \
		if (self->array[i].occupied) { \
			operation(self->array[i].the_key, self->array[i].the_value); \
		} \
	} \
} \
 \
static bool util_hash_##key##_##value##_realloc(struct util_hash_##key##_##value *self, uint32_t new_capacity) { \
	if (new_capacity != self->capacity) { \
		log_5("Resizing hashmap from %u to %u", self->capacity, new_capacity); \
 \
		struct util_hash_##key##_##value new_hash = util_hash_##key##_##value##_alloc_hash_equal(new_capacity, self->hash_function, self->is_equal); \
 \
		if (new_hash.array == NULL) { \
			log_1("Couldn't allocate memory to resize hash map from %u to %u", self->capacity, new_capacity); \
			return false; \
		} \
 \
		for (int i = 0; i < self->capacity; ++i) { \
			if (self->array[i].occupied) { \
				util_hash_##key##_##value##_add(&new_hash, self->array[i].the_key, self->array[i].the_value);	 \
			} \
		} \
 \
		free(self->array); \
		self->array = new_hash.array; \
		self->capacity = new_hash.capacity; \
	} \
 \
	return true; \
} \
 \
void util_hash_##key##_##value##_free(struct util_hash_##key##_##value *self) { \
	if (self->array != NULL) { \
		free(self->array); \
		self->array = NULL; \
	} \
	self->capacity = 0; \
} \

#include <util/hash.def> 

#undef create_hash_map
